%global apiversion 0.1

Summary: An EPUB generator library
Name: libepubgen
Version: 0.1.1
Release: 8%{?dist}

License: MPL-2.0
URL: https://sourceforge.net/projects/libepubgen/
Source0: http://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.xz

BuildRequires: make gcc-c++ pkgconfig(cppunit)
BuildRequires: boost-devel doxygen pkgconfig(librevenge-0.0)
BuildRequires: pkgconfig(libxml-2.0)

%description
%{name} is a library for generating EPUB documents. It is directly
pluggable into import filters based on librevenge.

%package devel
Summary: Development files for %{name}
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package doc
Summary: Documentation of %{name} API
BuildArch: noarch

%description doc
The %{name}-doc package contains documentation files for %{name}.

%prep
%setup -q

%build
%configure --disable-silent-rules --disable-static
sed -i \
    -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
    -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
    libtool
%make_build

%install
make install DESTDIR=%{buildroot} INSTALL='install -p'

rm -f %{buildroot}/%{_libdir}/*.la
rm -rf %{buildroot}/%{_docdir}/%{name}

%check
export LD_LIBRARY_PATH=%{buildroot}%{_libdir}${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
%make_build check


%files
%license COPYING
%doc AUTHORS README NEWS
%{_libdir}/%{name}-%{apiversion}.so.*

%files devel
%doc ChangeLog
%{_includedir}/%{name}-%{apiversion}
%{_libdir}/%{name}-%{apiversion}.so
%{_libdir}/pkgconfig/%{name}-%{apiversion}.pc

%files doc
%license COPYING
%doc docs/doxygen/html

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.1.1-8
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.1.1-7
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.1.1-6
- Rebuilt for OpenCloudOS Stream 23.09

* Tue Aug 22 2023 kianli@tencent.com - 0.1.1-5
- Rebuilt for librevenge 0.0.5

* Tue Aug 01 2023 rockerzhu rockerzhu@tencent.com - 0.1.1-4
- Rebuilt for boost 1.82.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.1.1-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.1.1-2
- Rebuilt for OpenCloudOS Stream 23

* Thu Mar 09 2023 cunshunxia <cunshunxia@tencent.com> - 0.1.1-1
- initial build
